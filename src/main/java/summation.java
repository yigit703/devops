public class summation
{
    public int sum( int i, int j)
    {
        return i + j;
    }

    public float sum( float i, float j)
    {
        return i + j;
    }

    public int sum( int i, int j, int k)
    {
        return i + j + k;
    }

    public float sum( float i, float j, float k)
    {
        return i + j + k;
    }
}
