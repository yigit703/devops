import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class summationTest
{
    private summation s = new summation();

    @Test
    public void checkSumInt(){
        assertEquals(3, s.sum(2, 1));
    }

    @Test
    public void checkSumFloat(){
        assertEquals(3,s.sum(1, 2 ) );
    }

    @Test
    public void checkSumInt3(){
        assertEquals(5, s.sum(2, 1, 2));
    }
}
